To test this, import the project and run 
```
#!maven

mvn -Dtest=MovieSearch test
```


This service is hosted at http://54.186.251.121:8080/omdb/

To signup, make a POST request to http://54.186.251.121:8080/omdb/users with a json object containing your name and emailID in the format

{
    "name": "Rajesh",
    "email": "rajesh@gmail.com"
}

the response will be a json object in the form

{
token: "sckf2kstvvaub6bfsnkgssgp10"
}

After signup, to search for a movie, make a GET request in the format http://54.186.251.121:8080/omdb/movies/Titanic?token=sckf2kstvvaub6bfsnkgssgp10

The response will be a json object with the movie details.

Each query will be stored in MySQL database on the same server.
To connect to the database you need to login into the server, using the key

ssh -i {key_location}/Mac.pem ubuntu@54.186.251.121